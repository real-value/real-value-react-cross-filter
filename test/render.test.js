import { describe } from 'riteway';
import render from 'riteway/render-component';
import React from 'react';

import { CrossFilter } from '../src';

describe('CrossFilter should render properly', async (assert) => {

  const createCrossFilter = () =>
    render(<CrossFilter data={[] } dimensions={[]} />)

    const $ = createCrossFilter();

    assert({
      given: 'a crossfilter component',
      should: 'Should render.',
      actual: $('div')
        .html()
        .trim(),
      expected: '<div> </div><table><thead><tr><th>Production</th><th>Asset</th></tr></thead><tbody></tbody></table>'
    });
});
