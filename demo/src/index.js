
import React, { useState, useEffect } from 'react';
import { render} from 'react-dom';
import { CrossFilter } from '../../src';
import  crossfilter2 from 'crossfilter2'

const xfilter = crossfilter2([])

let assetDimension = xfilter.dimension(function(d) { return d.asset })
let assetGroup = assetDimension.group()

let siteDimension = xfilter.dimension(function(d) { return d.site })
let siteGroup = siteDimension.group()

const App = () => {

const [data,setData] = useState([])

let dimensions =[
    {
        dimension: assetDimension,
        group: assetGroup,
        select: (value)=>{
            if(value){
                assetDimension.filter(value)
             }else {
                assetDimension.filterAll()
             }  
            setData(xfilter.allFiltered())
        }
    },
    {
        dimension: siteDimension,
        group: siteGroup,
        select: (value)=>{
            if(value){
                siteDimension.filter(value)
            }else {
                siteDimension.filterAll()
            }
            setData(xfilter.allFiltered())
        }
    }
]

    let cnt=1
    useEffect(()=>{
        let run = true
        function populate(){
            if(run){
                cnt++   
                xfilter.add([{production: cnt, asset: `Truck${cnt%3}`, site: `Site${cnt%4}`, timestamp: new Date()}])
                
                let data = xfilter.allFiltered()
                setData(data)
                setTimeout(populate,5000)    
            }
            return ()=>run=false
        }
        populate()
    },[setData])
    
    return <div>
        <h1>real-value-react-cross-filter Demo</h1>
        <CrossFilter data={data} dimensions={dimensions} />
    </div>
}
render(<App />, document.querySelector('#demo'));

