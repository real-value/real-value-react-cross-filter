import React, { useReducer } from 'react';

export const Group = (props) => {

  const { dimension, group, select } = props

  function filter(e){
    select(e.target.value)
  }

  let buttons = group.all().map((g,i)=>{
    return <button key={i} value={g.key} onClick={filter}>{g.key}</button>
  })
  return <div>
    <button key="-1" value={undefined} onClick={filter}>All</button>
    {buttons}
    </div> 
}
