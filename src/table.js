import React, { useReducer } from 'react';

export const Row = (props) => {
  const { entity } = props

  return <tr>
    <td>{entity.production}</td>
    <td>{entity.asset}</td>
  </tr>
}

export const Table = (props) => {
  const { data } = props

  return <table>
    <thead>
    <tr>
      <th>Production</th>
      <th>Asset</th>
    </tr>
    </thead>
    <tbody>
    {
      data.map((entity,i)=>{
        return <Row key={i} entity={entity}/>
    })
    }
    </tbody>
    </table> 
}
