import React from 'react';

import { Group } from './groups';
import { Table } from './table';

export const CrossFilterControls = (props) => {
  const { dimensions } = props
  return <div> {
    dimensions.map((d,i)=>{
      return <Group key={i} dimension={d.dimension} group={d.group} select={d.select}></Group>
    })
  }
  </div>

}
export const CrossFilter = (props) => {
  const { data, dimensions } = props

  return <div>
    <CrossFilterControls dimensions={dimensions}/> 
    <Table data={data} />
  </div> 
}
